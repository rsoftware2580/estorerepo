package com.nimap.estore.controller;

import java.util.Collections;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.nimap.estore.model.Category;
import com.nimap.estore.service.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {

	private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

	@Autowired
	private CategoryService categoryService;

	@GetMapping("/getAll")
	public @ResponseBody Iterable<Category> getAllCategory() throws Exception {
		logger.info("Category Rest Controller Implementation : getAllCategory() method");
		return categoryService.getAllCategory();
	}

	@PostMapping("/save")
	public ResponseEntity<Category> createCategory(@RequestBody Category category, HttpServletRequest request)
			throws Exception {
		Category createdCategory = this.categoryService.saveCategory(category);
		logger.info("Category Rest Controller Implementation : createCategory() method");
		return ResponseEntity.ok().body(createdCategory);
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<Category> getCategoryById(@PathVariable("id") Integer id) {
		Category category = categoryService.getCategoryById(id);
		logger.info("CAtegory Rest Controller Implementation : getCategoryById() method");
		return ResponseEntity.ok().body(category);
	}

	@DeleteMapping("/delete/{id}")
	public Map<String, String> deleteCategoryById(@PathVariable("id") Integer id) throws Exception {
		Category category = categoryService.getCategoryById(id);
		logger.info("Category Rest Controller Implementation : deleteCategoryById() method");
		if (category != null) {
			categoryService.deleteCategory(id);
			return Collections.singletonMap("success", "Record Deleted Successfully");
		} else {
			return Collections.singletonMap("fail!", "Something is wrong");
		}
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<Category> updateCategoryById(@PathVariable("id") Integer id, @RequestBody Category category) {
		Category categorys = categoryService.getCategoryById(id);
		categorys.setCategoryName(category.getCategoryName());
		Category category2 = categoryService.saveCategory(categorys);
		return ResponseEntity.ok().body(category2);
	}
}
