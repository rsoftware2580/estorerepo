package com.nimap.estore.serviceImpl;

import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nimap.estore.exception.EStoreException;
import com.nimap.estore.model.Category;
import com.nimap.estore.repository.CategoryRepository;
import com.nimap.estore.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	private CategoryRepository repo;

	@Override
	public Iterable<Category> getAllCategory() {
		logger.info("Category Service Implementation : getAllcategory() method");
		return repo.findAll();
	}

	@Override
	@Transactional
	public Category saveCategory(Category category) {
		logger.info("Product Service Implementation : saveProduct() method");
		return repo.save(category);
	}

	@Override
	public Category getCategoryById(Integer Id) {
		logger.info("Category Service Implementation : getCategoryById() method");
		Category category = repo.findById(Id);
		if (category == null) {
			throw new EStoreException("Category id " + Id + " incorrect..");
		}
		return category;
	}
		
	@Override
	public void deleteCategory(Integer id) {
		logger.info("Category Service Implementation : deleteCategory() method");
		repo.deleteById(id);
	}

}
