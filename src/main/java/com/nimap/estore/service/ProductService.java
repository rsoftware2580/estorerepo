package com.nimap.estore.service;

import java.util.List;

import com.nimap.estore.model.Product;

public interface ProductService {

	public List<Product> getAllProduct(Integer pageNo, Integer pageSize, String sortBy);

	public Product saveProduct(Product product);

	public <optional> Product getProductById(Integer Id);

	public void deleteProduct(Integer id);

	public Iterable<Product> getProduct();
}
