package com.nimap.estore.repository;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.nimap.estore.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Serializable> {

	public Product findById(Integer Id);

	@SuppressWarnings("unchecked")
	public Product save(Product product);
}
